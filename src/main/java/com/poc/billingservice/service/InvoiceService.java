package com.poc.billingservice.service;

import com.poc.billingservice.dto.InvoiceRequestDTO;
import com.poc.billingservice.dto.InvoiceResponseDTO;
import com.poc.billingservice.entity.Customer;
import com.poc.billingservice.entity.Invoice;
import com.poc.billingservice.mapper.InvoiceMapper;
import com.poc.billingservice.openfeign.CustomerRestClient;
import com.poc.billingservice.repository.InvoiceRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
public class InvoiceService implements IInvoiceService {

    private InvoiceRepository invoiceRepository;
    private InvoiceMapper invoiceMapper;
    private CustomerRestClient customerRestClient;

    public InvoiceService(InvoiceRepository invoiceRepository, InvoiceMapper invoiceMapper, CustomerRestClient customerRestClient) {
        this.invoiceRepository = invoiceRepository;
        this.invoiceMapper = invoiceMapper;
        this.customerRestClient = customerRestClient;
    }

    @Override
    public InvoiceResponseDTO addInvoice(InvoiceRequestDTO invoiceRequestDTO) {

        Invoice invoice = invoiceMapper.invoiceRequestDTOInvoice(invoiceRequestDTO);
        invoice.setDate(new Date());
        invoiceRepository.save(invoice);

        return invoiceMapper.invoiceToInvoiceResponseDTO(invoice);
    }

    @Override
    public List<InvoiceResponseDTO> getInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        for (Invoice invoice : invoices) {
            Customer c = customerRestClient.getCustomerById(invoice.getCustomerId());
            invoice.setCustomer(c);
        }

        return invoices.stream().map(invoice ->
                invoiceMapper.invoiceToInvoiceResponseDTO(invoice)).collect(Collectors.toList());
    }

    @Override
    public InvoiceResponseDTO getInvoiceById(Long invoiceId) {
        Invoice invoice = invoiceRepository.findById(invoiceId).orElse(null);
        assert invoice != null;
        Customer customer = customerRestClient.getCustomerById(invoice.getCustomerId());
        invoice.setCustomer(customer);

        return invoiceMapper.invoiceToInvoiceResponseDTO(invoice);
    }

    @Override
    public List<InvoiceResponseDTO> getInvoicesByCustomerId(Long customerId) {
        List<Invoice> invoices = invoiceRepository.findByCustomerId(customerId);
        for (Invoice invoice : invoices) {
            Customer c = customerRestClient.getCustomerById(invoice.getCustomerId());
            invoice.setCustomer(c);
        }

        return invoices.stream().map(invoice ->
                invoiceMapper.invoiceToInvoiceResponseDTO(invoice)).collect(Collectors.toList());
    }
}
