package com.poc.billingservice.service;

import com.poc.billingservice.dto.InvoiceRequestDTO;
import com.poc.billingservice.dto.InvoiceResponseDTO;
import com.poc.billingservice.entity.Invoice;

import java.util.List;

public interface IInvoiceService {

    InvoiceResponseDTO addInvoice(InvoiceRequestDTO invoiceRequestDTO);
    List<InvoiceResponseDTO> getInvoices();
    InvoiceResponseDTO getInvoiceById(Long invoiceId);
    List<InvoiceResponseDTO> getInvoicesByCustomerId(Long customerId);
}
