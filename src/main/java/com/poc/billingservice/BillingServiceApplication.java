package com.poc.billingservice;

import com.poc.billingservice.dto.InvoiceRequestDTO;
import com.poc.billingservice.service.InvoiceService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@EnableFeignClients
@SpringBootApplication
public class BillingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BillingServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(InvoiceService invoiceService) {
        return args -> {
            invoiceService.addInvoice(new InvoiceRequestDTO(1L, BigDecimal.valueOf(469874)));
            invoiceService.addInvoice(new InvoiceRequestDTO(1L, BigDecimal.valueOf(95658)));
            invoiceService.addInvoice(new InvoiceRequestDTO(2L, BigDecimal.valueOf(956686)));
        };
    }
}
