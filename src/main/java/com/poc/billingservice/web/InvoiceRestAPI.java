package com.poc.billingservice.web;


import com.poc.billingservice.dto.InvoiceRequestDTO;
import com.poc.billingservice.dto.InvoiceResponseDTO;
import com.poc.billingservice.service.InvoiceService;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping(path = "/api/invoices")
public class InvoiceRestAPI {

    private InvoiceService invoiceService;

    public InvoiceRestAPI(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping(path = "/")
    public List<InvoiceResponseDTO> getInvoices() {
        return invoiceService.getInvoices();
    }

    @PostMapping(path = "/add")
    public InvoiceResponseDTO addInvoice(@RequestBody InvoiceRequestDTO invoiceRequestDTO) {
        return invoiceService.addInvoice(invoiceRequestDTO);
    }

    @GetMapping(path = "/{id}")
    public InvoiceResponseDTO getCustomer(@PathVariable Long id) {
        return invoiceService.getInvoiceById(id);
    }

    @GetMapping(path = "/customer/{customerId}")
    public List<InvoiceResponseDTO> getInvoicesByCustomerId(@PathVariable Long customerId) {
        return invoiceService.getInvoicesByCustomerId(customerId);
    }
}
