package com.poc.billingservice.mapper;

import com.poc.billingservice.dto.InvoiceRequestDTO;
import com.poc.billingservice.dto.InvoiceResponseDTO;
import com.poc.billingservice.entity.Invoice;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InvoiceMapper {

    Invoice invoiceRequestDTOInvoice(InvoiceRequestDTO invoiceRequestDTO);
    InvoiceResponseDTO invoiceToInvoiceResponseDTO(Invoice i);
}
