package com.poc.billingservice.dto;

import com.poc.billingservice.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data @NoArgsConstructor @AllArgsConstructor
public class InvoiceResponseDTO {

    private Long id;
    private Date date;
    private BigDecimal amount;

    private Customer customer;
}
